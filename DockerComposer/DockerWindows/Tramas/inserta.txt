  private final String INSERTA_Persona="INSERT INTO Persona(Cedula,Nombre,Apellido1,Apellido2,Fech_Nac,Edad) Values(?,?,?,?,?,?)";
    private final String INSETA_Monitor=" INSERT INTO Monitor(IP,Num_Cama,FechaRegistro,Ced_Pac) Values(?,?,?,?)";
    private final String INSERTASQL_ECG="INSERT INTO ECG(ID_Indicador,HoraSenal) Values(?,?)";
    private final String INSERTASQL_SPO2="INSERT INTO SPO2( ID_Indicador,HoraSenal,Frec_Cardi,Desconocido)Values(?,?,?,?)";
    private final String INSERTASQL_FrecRespira="INSERT INTO Frec_Respiratoria(ID_Indicador,HoraSenal,Impedancia)Values(?,?,?)";
    private final String INSERTASQL_Senal_Roja="INSERT INTO Senal_Roja(ID_Indicador,HoraSenal,Maximo,Minimo,Parentesis) Values(?,?,?,?,?)";
    private final String INSERTASQL_Senal_Amarilla="INSERT INTO Senal_Amarilla(ID_Indicador,HoraSenal) Values(?,?)";
    private final String INSERTASQL_Temperatura="INSERT INTO Temperatura(ID_Indicador,HoraSenal) Values(?,?)";
    private final String INSERTASQL_Genera="INSERT INTO Genera(FechaSeņal,IP,Cedula,FechaRegistro,HoraSenal,ID_Indicador1,ID_Indicador2,ID_Indicador3,ID_Indicador4,ID_Indicador5,ID_Indicador6) Values(?,?,?,?,?,?,?,?,?,?,?)";
    
     public int insertaPersona(int Cedula,String Nombre,String Apellido1,String Apellido2,Date Fech_Nac,int Edad){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTA_Persona);
            int index=1;
            stat.setInt(index++, Cedula);
            stat.setString(index++, Nombre);
            stat.setString(index++, Apellido1);
            stat.setString(index++, Apellido2);
            stat.setDate(index++, Fech_Nac);
            stat.setInt(index++, Edad);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
    }
     
      public int insertaMonitor(int IP,int Num_Cama,Date FechaRegistro,int Ced_Pac){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSETA_Monitor);
            int index=1;
            stat.setInt(index++, IP);
            stat.setInt(index++, Num_Cama);
            stat.setDate(index++, FechaRegistro);
            stat.setInt(index++, Ced_Pac);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
    }
      
       public int insertaGenera(Date FechaSeņal,int IP,int Cedula,Date FechaRegistro,Time HoraSenal,int ID_Indicador1,int ID_Indicador2,int ID_Indicador3,int ID_Indicador4,int ID_Indicador5,int ID_Indicador6){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTASQL_Genera);
            int index=1;
            stat.setDate(index++, FechaSeņal);
            stat.setInt(index++, IP);
            stat.setInt(index++, Cedula);
            stat.setDate(index++, FechaRegistro);
            stat.setTime(index++, HoraSenal);
            stat.setInt(index++, ID_Indicador1);
            stat.setInt(index++, ID_Indicador2);
            stat.setInt(index++, ID_Indicador3);
            stat.setInt(index++, ID_Indicador4);
            stat.setInt(index++, ID_Indicador5);
            stat.setInt(index++, ID_Indicador6);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
    }
      
    
    public int insertaECG(int ID_Indicador,Time HoraSenal){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTASQL_ECG);
            int index=1;
            stat.setInt(index++, ID_Indicador);
            stat.setTime(index++, HoraSenal);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
    }
    
   public int insertaSPO2(int ID_Indicador,Time HoraSenal,int Frec_Cardi,float Desconocido){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTASQL_SPO2);
            int index=1;
            stat.setInt(index++, ID_Indicador);
            stat.setTime(index++, HoraSenal);
            stat.setInt(index++, Frec_Cardi);
            stat.setFloat(index++, Desconocido);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
   
   }
    public int insertaFrecRespira(int ID_Indicador,Time HoraSenal,float Impedancia){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTASQL_FrecRespira);
            int index=1;
            stat.setInt(index++, ID_Indicador);
            stat.setTime(index++, HoraSenal);
            stat.setFloat(index++,Impedancia );
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
   
   }
    
    public int insertaSenalRoja(int ID_Indicador,Time HoraSenal,float Maximo,float Minimo,float Parentesis){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTASQL_Senal_Roja);
            int index=1;
            stat.setInt(index++, ID_Indicador);
            stat.setTime(index++, HoraSenal);
            stat.setFloat(index++,Maximo);
            stat.setFloat(index++,Minimo);
            stat.setFloat(index++,Parentesis);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
   
   } 
   public int insertaSenalAmarilla(int ID_Indicador,Time HoraSenal){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTASQL_Senal_Amarilla);
            int index=1;
            stat.setInt(index++, ID_Indicador);
            stat.setTime(index++, HoraSenal);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
   
   }  
   
    public int insertaTemperatura(int ID_Indicador,Time HoraSenal){
        Connection cos=null;
        PreparedStatement stat=null;
        //Conexion.getConexion();
        int rowns=0; 
        try{
            cos=Conexion.getConecxion();
            stat=cos.prepareStatement(INSERTASQL_Temperatura);
            int index=1;
            stat.setInt(index++, ID_Indicador);
            stat.setTime(index++, HoraSenal);
            rowns=stat.executeUpdate();
        }catch(SQLException ex){
            ex.printStackTrace();
        }finally{
            Conexion.close(cos);
            Conexion.close(stat);
            return rowns;
        }
   
   }  
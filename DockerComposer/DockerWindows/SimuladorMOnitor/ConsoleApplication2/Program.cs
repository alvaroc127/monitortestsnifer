﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;            
using System.Net.Sockets;       
//////////// servidor
namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)

        {
            Conectar();  

        }
        public static void Conectar()
        {
            byte[] ByRec;
            Socket miPrimerSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint miDireccion = new IPEndPoint(IPAddress.Any, 12345);
            try
            {
                miPrimerSocket.Bind(miDireccion);
                miPrimerSocket.Listen(17);

                Console.WriteLine("Escuchando...");
                Socket Escuchar = miPrimerSocket.Accept();
                Console.WriteLine("Conectado con exito");
                int band = 0;
                while (band == 0)
                {
                    ByRec = new byte[5000];
                    int a = Escuchar.Receive(ByRec, 0, ByRec.Length, 0);
                    Array.Resize(ref ByRec, a);
                    Console.WriteLine("Cliente dice: " + Encoding.Default.GetString(ByRec)); //mostramos lo recibido
                    //System.Console.WriteLine("valor de a"+a);
                }
                miPrimerSocket.Close();

            }
            catch (Exception error)
            {
                Console.WriteLine("Error: {0}", error.ToString());
            }
            Console.WriteLine("Presione cualquier tecla para terminar");
            Console.ReadLine();
        }
    }
}
